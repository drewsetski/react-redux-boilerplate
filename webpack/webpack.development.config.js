const path = require('path');
const config = require('./webpack.common.config.js');
const webpack = require('webpack');

config.entry.push('webpack-dev-server/client?http://localhost:3000',
    'webpack/hot/dev-server',
    'react-hot-loader/patch'
);
config.output = {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/public'
};
config.plugins = [new webpack.HotModuleReplacementPlugin()];
config.devtool = 'eval-source-map';

// All the styling loaders only apply to hot-reload, not rails
config.module.loaders.push(
    {test: /\.js?$/, loaders: ['babel'], exclude: /node_modules/},
    {test: /\.css$/, loader: 'style-loader!css-loader'},
    {
        test: /\.scss$/,
        loader: 'style!css!sass?outputStyle=expanded&imagePath=assets/images&includePaths[]=' +
        path.resolve(__dirname, '../src/assets/styles')
    },

    // The url-loader uses DataUrls. The file-loader emits files.
    {
        test: /\.woff(\?v=\d+\.\d+\.\d+|\?.*)?$/,
        loader: 'url?limit=10000&mimetype=application/font-woff',
    },
    {
        test: /\.woff2(\?v=\d+\.\d+\.\d+|\?.*)?$/,
        loader: 'url?limit=10000&mimetype=application/font-woff',
    },
    {
        test: /\.ttf(\?v=\d+\.\d+\.\d+|\?.*)?$/,
        loader: 'url?limit=10000&mimetype=application/octet-stream',
    },
    {
        test: /\.eot(\?v=\d+\.\d+\.\d+|\?.*)?$/,
        loader: 'file',
    },
    {
        test: /\.svg(\?v=\d+\.\d+\.\d+|\?.*)?$/,
        loader: 'url?limit=10000&mimetype=image/svg+xml',
    }
);

module.exports = config;