const path = require('path');

module.exports = {
    context: __dirname,
    presets:['react'],
    entry: ['../src/index'],
    resolve: {
        root: path.join(__dirname, 'src'),
        extensions: ['', '.webpack.js', '.web.js', '.js', '.jsx', '.scss', '.css', 'config.js']
    },
    module: {
        loaders: [],
    },
    plugins: []
};