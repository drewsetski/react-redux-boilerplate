const path = require('path');
const config = require('./webpack.common.config.js');
const webpack = require('webpack');


// We're using the bootstrap-sass loader.
// See: https://github.com/justin808/bootstrap-sass-loader
config.output = {
    path: path.join(__dirname, '../public'),
    filename: 'bundle.js',
};

config.plugins = config.plugins.concat([
    new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: JSON.stringify('production')
        }
    })
])

// All the styling loaders only apply to hot-reload, not rails
config.module.loaders.push(
    {test: /\.jsx?$/, loaders: ['babel'], exclude: /node_modules/},
    {test: /\.css$/, loader: 'style-loader!css-loader'},
    {
        test: /\.scss$/,
        loader: 'style!css!sass?outputStyle=expanded&imagePath=assets/images&includePaths[]=' +
        path.resolve(__dirname, '../src/assets/styles')
    },

    // The url-loader uses DataUrls. The file-loader emits files.
    {
        test: /\.woff(\?v=\d+\.\d+\.\d+|\?.*)?$/,
        loader: 'url?limit=10000&mimetype=application/font-woff',
    },
    {
        test: /\.woff2(\?v=\d+\.\d+\.\d+|\?.*)?$/,
        loader: 'url?limit=10000&mimetype=application/font-woff',
    },
    {
        test: /\.ttf(\?v=\d+\.\d+\.\d+|\?.*)?$/,
        loader: 'url?limit=10000&mimetype=application/octet-stream',
    },
    {
        test: /\.eot(\?v=\d+\.\d+\.\d+|\?.*)?$/,
        loader: 'file',
    },
    {
        test: /\.svg(\?v=\d+\.\d+\.\d+|\?.*)?$/,
        loader: 'url?limit=10000&mimetype=image/svg+xml',
    }
);

module.exports = config;