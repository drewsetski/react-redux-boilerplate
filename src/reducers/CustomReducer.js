import * as types from '../constants/ActionTypes';
import {RESOLUTIONS} from "../constants/Canvas";

export default function customReducer(state = {
    label: null
}, action) {
    switch (action.type) {
        case types.ACTION_TYPE:
            return {
                ...state,
                label: action.label
            }
        default:
            return state;
    }
}