import {combineReducers} from 'redux';
import customReducer from './CustomReducer';

const rootReducer = combineReducers({
    customReducer,
});

export default rootReducer;