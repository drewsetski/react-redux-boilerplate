import 'react-hot-loader/patch';
import React from 'react';
import ReactDOM from 'react-dom';
import configureStore from './store/configureStore';
import {AppContainer} from 'react-hot-loader';
require('./assets/styles/styles.scss');

import Root from './views/Root';

const store = configureStore();

ReactDOM.render(
    <AppContainer>
        <Root
            store={ store }
        />
    </AppContainer>,
    document.getElementById('app')
);

if (module.hot) {
    module.hot.accept('./views/Root', () => {
        const RootContainer = require('./views/Root').default;
        ReactDOM.render(
            <AppContainer>
                <RootContainer
                    store={ store }
                />
            </AppContainer>,
            document.getElementById('app')
        );
    });
}