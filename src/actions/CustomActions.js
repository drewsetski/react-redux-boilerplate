import * as types from "../constants/ActionTypes";

export function customAction(label) {
    return {
        type: types.ACTION_TYPE,
        label
    }
}