import React from 'react';
import Helmet from 'react-helmet';
const TabPane = Tabs.TabPane;
import * as customActions from '../actions/CustomActions';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";


class App extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.actions.customAction('world');
    }


    render() {
        return (
            <div>Hello {{this.props.label}}!</div>
        );
    }

}

function mapStateToProps(state) {
    return {
        label: state.customReducer.label
    }
}

function mapDispatchToProps(dispatch) {
    return {actions: bindActionCreators({...customActions}, dispatch)}
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
